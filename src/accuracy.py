import neuralnet.net as nn
import nnfs
import numpy as np
from nnfs.datasets import spiral_data

nnfs.init()


X, y = spiral_data(samples=100, classes=3)


dense1 = nn.Layer_Dense(2, 3)
activation1 = nn.Activation_ReLU()

dense2 = nn.Layer_Dense(3, 3)
activation2 = nn.Activation_Softmax()


dense1.forward(X)
activation1.forward(dense1.output)
dense2.forward(activation1.output)

activation2.forward(dense2.output)

print('\n')
print(activation2.output[:5])

floss = nn.Loss_CCE()

loss = floss.calculate(activation2.output, y)
print(loss)

# Calculate accuracy from output of activation2 and targets
# calculate values along first axis
predictions = np.argmax(activation2.output, axis=1)
if len(y.shape) == 2:
 y = np.argmax(y, axis=1)
accuracy = np.mean(predictions==y)
# Print accuracy
print('acc:', accuracy)

