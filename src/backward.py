import neuralnet as nn
import nnfs
import numpy as np
from timeit import timeit
from nnfs.datasets import spiral_data

nnfs.init()



softmax_outputs = np.array([[0.7, 0.1, 0.2, 0.3],
[0.1, 0.5, 0.4, 0.7],
[0.1, 0.5, 0.4, 0.7],
[0.1, 0.5, 0.4, 0.7],
[0.1, 0.5, 0.4, 0.7],
[0.02, 0.9, 0.08, 0.9]])

class_targets = np.array([0, 1, 1, 1, 1, 1])

def f1():
    softmax_loss = nn.Activation_Softmax_Loss_CCE()
    softmax_loss.backward(softmax_outputs, class_targets)
    dvalues1 = softmax_loss.dinputs

def f2():
    activation = nn.Activation_Softmax()
    activation.output = softmax_outputs
    loss = nn.Loss_CCE()
    loss.backward(softmax_outputs, class_targets)
    activation.backward(loss.dinputs)
    dvalues2 = activation.dinputs


loss_activ = nn.Activation_Softmax_Loss_CCE()
loss_function = nn.Loss_CCE()



t1 = timeit(lambda: f1(), number=10000)
t2 = timeit(lambda: f2(), number=10000)
print('\n')
print(t2/t1)
