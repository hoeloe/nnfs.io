import neuralnet as nn
import nnfs
import numpy as np
from termcolor import colored, cprint
from nnfs.datasets import spiral_data
from numba import cuda

import matplotlib.pyplot as plt

nnfs.init()


X, y = spiral_data(samples=100, classes=2)

y = y.reshape(-1, 1)


# init objects
dense1 = nn.Layer_Dense(2, 64, weight_regularizer_l2=5e-4, bias_regularizer_l2=5e-4)
activation1 = nn.Activation_ReLU()


dense2 = nn.Layer_Dense(64, 1)
activation2 = nn.Activation_Sigmoid()

loss_function = nn.Loss_BCE()

# optimizer = nn.Optimizer_Adagrad(decay=1e-4)
# optimizer = nn.Optimizer_RMSprop(decay=1e-4)
optimizer = nn.Optimizer_Adam(decay=5e-7)
# optimizer = nn.Optimizer_SGD(decay=1e-3, momentum=0.9)


epochs = {}

losses = []
accs = []
lrs = []
fig, (ax1, ax2, ax3) = plt.subplots(3,1)
ax1.set_title('loss')
ax2.set_title('accuracy')
ax3.set_title('learning rate')



# forward pass
for epoch in range(5001):

   dense1.forward(X)

   activation1.forward(dense1.output)

   dense2.forward(activation1.output)

   activation2.forward(dense2.output)

   data_loss = loss_function.calculate(activation2.output, y)

   # Calculate regularization penalty
   regularization_loss = \
      loss_function.regularization_loss(dense1) + \
      loss_function.regularization_loss(dense2)
   # Calculate overall loss
   loss = data_loss + regularization_loss
   # calc combined losses

   # Calculate accuracy from output of activation2 and targets
   # calculate values along first axis
   predictions = (activation2.output > 0.5) * 1
   if len(y.shape) == 2:
      y = np.argmax(y, axis=1)
   accuracy = np.mean(predictions==y)



   # Print accuracy
   if not epoch % 100 or epoch < 10:
      data = {'loss': loss, 'acc': accuracy, 'epoch': epoch}
      epochs[epoch] = data
      print(f'epoch: {epoch}, ' +
            f'acc: {accuracy:.3f}, ' +
            f'loss: {loss:.3f} ' +
            f'lr: {optimizer.current_learning_rate}')

   # save data
   losses.append(loss)
   accs.append(accuracy)
   lrs.append(optimizer.current_learning_rate)

   # backward pass
   loss_function.backward(activation2.output, y)
   activation2.backward(loss_function.dinputs)
   dense2.backward(activation2.dinputs)
   activation1.backward(dense2.dinputs)
   dense1.backward(activation1.dinputs)


   # optimize
   optimizer.pre_update_params()
   optimizer.update_params(dense1)
   optimizer.update_params(dense2)
   optimizer.post_update_params()


# testing

x_test, y_test = spiral_data(samples=100, classes=2)

y_test = y_test.reshape(-1, 1)

dense1.forward(x_test)

activation1.forward(dense1.output)

dense2.forward(activation1.output)

activation2.forward(dense2.output)

loss_test = loss_function.calculate(dense2.output, y_test)

# Calculate accuracy from output of activation2 and targets
# calculate values along first axis
predictions_test = (activation2.output > 0.5) * 1
if len(y.shape) == 2:
   y = np.argmax(y, axis=1)
accuracy_test = np.mean(predictions_test==y_test)

print(f'\n acc: {accuracy_test}, \n loss: {loss_test}')



# plotting


ax1.plot(losses)
ax2.plot(accs)
ax3.plot(lrs)

plt.show()
