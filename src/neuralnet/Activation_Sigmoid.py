import numpy as np

class Activation_Sigmoid:
    def forward(self, inputs):
   # Save input and calculate/save output
   # of the sigmoid function
        self.inputs = inputs
        self.output = 1 / (1 + np.exp(-inputs))
        print('output')
        print(self.output.shape)
   # Backward pass
    def backward(self, dvalues):
   # Derivative - calculates from output of the sigmoid function
        self.dinputs = dvalues * (1 - self.output) * self.output
        print(self.dinputs.shape)
