import numpy as np
from .Activation_Softmax import Activation_Softmax
from .Loss_CCE import Loss_CCE

class Activation_Softmax_Loss_CCE():

    def __init__(self):
        self.activation = Activation_Softmax()
        self.loss = Loss_CCE()

    def forward(self, inputs, y_true):
        self.activation.forward(inputs)
        self.output = self.activation.output
        return self.loss.calculate(self.output, y_true)

    def backward(self, dvalues, y_true):
        samples = len(dvalues)

        # turn one-hot labels into discrete values
        if len(y_true.shape) == 2:
            y_true = np.argmax(y_true, axis=1)

        self.dinputs = dvalues.copy()
        #calc gradient
        self.dinputs[range(samples), y_true] -= 1
        self.dinputs = self.dinputs / samples
