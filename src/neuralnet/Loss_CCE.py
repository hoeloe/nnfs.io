import numpy as np
from .Loss import Loss


class Loss_CCE(Loss):
    def forward(self, y_pred, y_true):
        samples = len(y_pred)
        y_pred_clipped = np.clip(y_pred, 1e-7, 1-1e-7)

        # check for one-hot vector data
        if len(y_true.shape) == 1:
            correct_confidences = y_pred_clipped[range(samples), y_true]

        elif len(y_true.shape) == 2:
            correct_confidences = np.sum(y_pred_clipped*y_true, axis=1)

        negative_log_likelyhoods = -np.log(correct_confidences)
        return negative_log_likelyhoods

    def backward(self, dvalues, y_true):
        # number of samples
        samples = len(dvalues)
        #number of labels
        labels = len(dvalues[0])

        # make sure of one hot vector data
        if len(y_true.shape) == 1:
            y_true = np.eye(labels)[y_true]

        self.dinputs = -y_true / dvalues
        #normalize gradient
        self.dinputs = self.dinputs / samples
