import numpy as np

class Optimizer_SGD:

    def __init__(self, learning_rate=1., decay=0., momentum=0):
        self.lr = learning_rate
        self.lr_current = learning_rate
        self.decay = decay
        self.iterations = 0
        self.momentum = momentum

    def pre_update_params(self):

        if self.decay:
            self.lr_current = self.lr * (1. / (1. + self.decay * self.iterations))

    def update_params(self, layer):

        # if we use momentum, update weights and biases with it
        if self.momentum:

            if not hasattr(layer, 'weight_momentums'):
                layer.weight_momentums = np.zeros_like(layer.weights)
                layer.bias_momentums = np.zeros_like(layer.biases)

            weight_updates = self.momentum * layer.weight_momentums - self.lr_current * layer.dweights
            layer.weight_momentums = weight_updates

            bias_updates = self.momentum * layer.bias_momentums - self.lr_current * layer.dbiases
            layer.bias_momentums = bias_updates

        else:
            weight_updates = -self.lr_current * layer.dweights
            bias_updates = -self.lr_current * layer.dbiases

        layer.weights += weight_updates
        layer.biases += bias_updates

    def post_update_params(self):
        self.iterations += 1
