import numpy as np
from numba import cuda




@cuda.jit
def cudakernel0(array):
    for i in range(array.size):
        array[i] += 0.5


array = np.array([0, 1, 2 ,3, 5], np.float32)
print('Initial array:', array)

print('Kernel launch: cudakernel0[1, 1](array)')
cudakernel0[5, 5](array)

print('Updated array:',array)



class Cudalicious:


    @cuda.jit
    def forward(self, inputs):


        self.output = np.dot(inputs, )
        self.inputs = inputs


    def backward(self, dvalues):
        # gradient on parameters:
        self.dweights = np.dot(self.inputs.T, dvalues)
        self.dbiases = np.sum(dvalues, axis=0, keepdims=True)

        # Gradients on regularization
        # L1 on weights
        if self.weight_regularizer_l1 > 0:
            dL1 = np.ones_like(self.weights)
            dL1[self.weights < 0] = -1
            self.dweights += self.weight_regularizer_l1 * dL1
        # L2 on weights
        if self.weight_regularizer_l2 > 0:
            self.dweights += 2 * self.weight_regularizer_l2 * \
                             self.weights
        # L1 on biases
        if self.bias_regularizer_l1 > 0:
            dL1 = np.ones_like(self.biases)
            dL1[self.biases < 0] = -1
            self.dbiases += self.bias_regularizer_l1 * dL1
        # L2 on biases
        if self.bias_regularizer_l2 > 0:
            self.dbiases += 2 * self.bias_regularizer_l2 * \
                            self.biases

        # Gradient on values
        self.dinputs = np.dot(dvalues, self.weights.T)
#
# layer = Cudalicious(2, 8)
# layer.forward([0.3,0.9])