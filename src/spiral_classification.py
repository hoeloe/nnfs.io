import neuralnet as nn
import nnfs
import numpy as np
from termcolor import colored, cprint
from nnfs.datasets import spiral_data
from numba import cuda

import matplotlib.pyplot as plt

nnfs.init()


X, y = spiral_data(samples=100, classes=3)

x_test, y_test = spiral_data(samples=100, classes=3)

# init objects
dense1 = nn.Layer_Dense(2, 64, weight_regularizer_l2=5e-4, bias_regularizer_l2=5e-4)
activation1 = nn.Activation_ReLU()

dropout1 = nn.Layer_Dropout(0.1)

dense2 = nn.Layer_Dense(64, 3)
loss_activation = nn.Activation_Softmax_Loss_CCE()

# optimizer = nn.Optimizer_Adagrad(decay=1e-4)
# optimizer = nn.Optimizer_RMSprop(decay=1e-4)
optimizer = nn.Optimizer_Adam(learning_rate=0.05, decay=5e-5)
# optimizer = nn.Optimizer_SGD(decay=1e-3, momentum=0.9)


epochs = {}

losses = []
accs = []
lrs = []
fig, (ax1, ax2, ax3) = plt.subplots(3,1)
ax1.set_title('loss')
ax2.set_title('accuracy')
ax3.set_title('learning rate')



# forward pass
for epoch in range(5001):

   dense1.forward(X)

   activation1.forward(dense1.output)

   dropout1.forward(activation1.output)

   dense2.forward(activation1.output)

   # calc combined losses
   data_loss = loss_activation.forward(dense2.output, y)

   regularization_loss = loss_activation.loss.regularization_loss(dense1) + \
                         loss_activation.loss.regularization_loss(dense2)

   loss = data_loss + regularization_loss

   # Calculate accuracy from output of activation2 and targets
   # calculate values along first axis
   predictions = np.argmax(loss_activation.output, axis=1)
   if len(y.shape) == 2:
      y = np.argmax(y, axis=1)
   accuracy = np.mean(predictions==y)



   # Print accuracy
   if not epoch % 100 or epoch < 10:
      data = {'loss': loss, 'acc': accuracy, 'epoch': epoch}
      epochs[epoch] = data
      print(f'epoch: {epoch}, ' +
            f'acc: {accuracy:.3f}, ' +
            f'loss: {loss:.3f} ' +
            f'data loss: {data_loss:.3f} ' +
            f'regularization loss: {regularization_loss:.3f} ' +
            f'lr: {optimizer.current_learning_rate}')

   # save data
   losses.append(loss)
   accs.append(accuracy)
   lrs.append(optimizer.current_learning_rate)

   # backward pass
   loss_activation.backward(loss_activation.output, y)
   dense2.backward(loss_activation.dinputs)
   dropout1.backward(dense2.dinputs)
   activation1.backward(dense2.dinputs)
   dense1.backward(activation1.dinputs)


   # optimize
   optimizer.pre_update_params()
   optimizer.update_params(dense1)
   optimizer.update_params(dense2)
   optimizer.post_update_params()


# testing


dense1.forward(x_test)

activation1.forward(dense1.output)

dense2.forward(activation1.output)

loss_test = loss_activation.forward(dense2.output, y)

# Calculate accuracy from output of activation2 and targets
# calculate values along first axis
predictions_test = np.argmax(loss_activation.output, axis=1)
if len(y.shape) == 2:
   y = np.argmax(y, axis=1)
accuracy_test = np.mean(predictions==y)

print(f'\n acc: {accuracy_test}, \n loss: {loss_test}')



# plotting


ax1.plot(losses)
ax2.plot(accs)
ax3.plot(lrs)

plt.show()
